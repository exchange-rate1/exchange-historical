import { HttpService, Injectable, Logger } from '@nestjs/common';
import { AppConfigService } from './app-config/app-config.service';
import { GeneralException } from './shared/exceptions/handlers';
import { ApiErrors } from './shared/response/messages';
import { GetExRateResponse } from './types';

@Injectable()
export class AppService {
  constructor(
    private readonly httpService: HttpService,
    private readonly appConfigService: AppConfigService,
  ) {}

  async getHistoricalExchangeRates(date: string): Promise<GetExRateResponse> {
    try {
      const response = await this.httpService
        .get<GetExRateResponse>(
          `${this.appConfigService.openExchangeRatesBaseUrl}/historical/${date}.json?app_id=${this.appConfigService.openExchangeRates}`,
          {
            // transformation of json response from third-party API to Object.
            transformResponse: (response): GetExRateResponse => {
              const parsedResponse = JSON.parse(response);
              return {
                timestamp: parsedResponse.timestamp,
                base: parsedResponse.base,
                rates: parsedResponse.rates,
              };
            },
          },
        )
        .toPromise();
      return response.data;
    } catch (error: any) {
      Logger.error(error);
      throw new GeneralException(ApiErrors.SystemErrorMessage);
    }
  }
}
