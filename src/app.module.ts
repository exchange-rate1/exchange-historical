import { HttpModule, Module } from '@nestjs/common';
import { AppConfigModule } from './app-config/app-config.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [AppConfigModule, HttpModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
