import { HttpException } from '@nestjs/common';
import { ApiErrors } from '../../response/messages';

export class SystemException extends HttpException {
  constructor(error = ApiErrors.SystemErrorMessage) {
    super(error, error.httpStatus);
  }
}
