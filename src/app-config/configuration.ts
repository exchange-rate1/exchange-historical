export default () => ({
  port: parseInt(process.env.PORT) || 3000,
  openExchangeRates: {
    apiKey: process.env.OER_API_KEY,
    baseUrl: process.env.OER_BASE_URL,
  },
});
