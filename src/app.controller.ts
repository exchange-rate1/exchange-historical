import { Controller, Get } from '@nestjs/common';
import { EventPattern, MessagePattern } from '@nestjs/microservices';
import { AppService } from './app.service';

@Controller('/')
export class AppController {
  constructor(private readonly appService: AppService) {}
  @Get('/ping')
  ping() {
    return { text: 'pong' };
  }

  // to get list of historical Exchange Rates.
  @MessagePattern('get_historical_data')
  async getHistoricalExchangeRates(date: string) {
    const temp = await this.appService.getHistoricalExchangeRates(date);
    return temp;
  }
}
