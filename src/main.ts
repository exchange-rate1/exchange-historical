import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';
import {
  GeneralExceptionFilter,
  ValidationExceptionFilter,
} from './shared/exceptions/filters';

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.TCP,
    options: { host: process.env.HOST, port: process.env.PORT },
  });

  app.useGlobalFilters(
    new GeneralExceptionFilter(),
    new ValidationExceptionFilter(),
  );
  app.listen(() => console.log('Exchange-Historical service is listening'));
}
bootstrap();
