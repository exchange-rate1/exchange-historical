export type GetExRateResponse = {
  base: string;
  timestamp: number;
  rates: { [key: string]: number };
};
