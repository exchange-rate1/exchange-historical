# Description
This is the Exchange-Historical microservice which has been used at the backend of Exchange Rates Service (link below). The primary purpose of this microservice is to provide the backend server with the list of historical exchange rates based on date input. 

### Links to other related repositories

- Hosted on: [HerokuApp with AWS EC2 backend](https://exrate-app.herokuapp.com/)
- Repo Group: [exchange-rate1](https://gitlab.com/exchange-rate1)
- Backend: [exchange-rate](https://gitlab.com/exchange-rate1/exchange-rate) 
- Microservices: [exchange-latest](https://gitlab.com/exchange-rate1/exchange-latest) [exchange-historical](https://gitlab.com/exchange-rate1/exchange-historical)




## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
